from mathutil.Rotation2d import Rotation2d
from mathutil.Translation2d import Translation2d


class Pose2d:
    translation = Translation2d()
    rotation = Rotation2d()

    def __init__(self, trans=Translation2d(), rot=Rotation2d()):
        self.translation = trans
        self.rotation = rot

    @property
    def x(self):
        return self.translation.x

    @property
    def y(self):
        return self.translation.y

    @property
    def cos(self):
        return self.rotation.cos

    @property
    def sin(self):
        return self.rotation.sin

    @property
    def theta(self):
        return self.rotation.radians

    def transformBy(self, other):
        return Pose2d(
            self.translation.translateBy(other.translation),
            self.rotation.rotateBy(other.rotation)
        )

    @property
    def inverse(self):
        return Pose2d(
            self.translation.inverse().rotateByOrigin(self.rotation.inverse), self.rotation.inverse
        )

